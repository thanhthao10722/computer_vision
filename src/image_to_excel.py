import os
import pandas as pd
from helper import write_to_excel, box_detect, predict_value
from cfg import (
    DETECT_LINE_PATH,
    DETECT_LETTER_PATH,
    OUTPUT_PATH,
    EXCEL_RESULT,
    FILE_EXCEL_RESULT
)


def image_to_excel(
    img_for_box_extraction_path, result_dir_path, result_excel
):
    list_path = [OUTPUT_PATH, result_dir_path, DETECT_LINE_PATH,
                 EXCEL_RESULT, DETECT_LETTER_PATH]
    for child in list_path:
        if not os.path.isdir(child):
            os.mkdir(child)
    contours, img_blur = box_detect(img_for_box_extraction_path)
    result = predict_value(contours, img_blur, result_dir_path)
    write_to_excel(result, result_excel)


def submit_result(header_path, image_path, result_path):
    image_to_excel(
        image_path,
        DETECT_LETTER_PATH,
        FILE_EXCEL_RESULT
    )
    df_header = pd.read_excel(header_path, header=None, encoding="utf-8")
    df_result = pd.read_excel(FILE_EXCEL_RESULT, header=None, encoding="utf-8")
    df = df_header.append(df_result)

    df.to_excel(
        result_path, index=False, header=None, encoding="utf-8")
