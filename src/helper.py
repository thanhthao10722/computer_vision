import cv2
from cfg import (
    SEGMENTATION_RESULT,
    DETECT_LINE_PATH,
    NUMBER_MAPPING
)
import numpy as np
import pandas as pd
from cnn_model import model
import os


def box_detect(img_for_box_extraction_path):
    img = cv2.imread(img_for_box_extraction_path, 1)  # Read the image
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # img_blur = cv2.medianBlur(img,5).astype('uint8')
    (thresh, img_blur) = cv2.threshold(imgGray, 150, 255,
                                       cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    img_bin = 255 - img_blur  # Invert the image

    cv2.imwrite(
        "{}/Image_bin.jpg".format(DETECT_LINE_PATH),
        img_bin
    )
    # Defining a kernel length
    kernel_length = np.array(img).shape[1] // 40

    verticle_kernel = cv2.getStructuringElement(
        cv2.MORPH_RECT, (1, kernel_length))
    # print(verticle_kernel)
    hori_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_length, 1))

    # A kernel of (3 X 3) ones.
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    # Morphological operation to detect verticle lines from an image
    img_temp1 = cv2.erode(img_bin, verticle_kernel, iterations=2)
    verticle_lines_img = cv2.dilate(img_temp1, verticle_kernel, iterations=2)
    cv2.imwrite(
        "{}/verticle_lines.jpg".format(DETECT_LINE_PATH),
        verticle_lines_img
    )
    # Morphological operation to detect horizontal lines from an image
    img_temp2 = cv2.erode(img_bin, hori_kernel, iterations=2)
    horizontal_lines_img = cv2.dilate(img_temp2, hori_kernel, iterations=2)
    cv2.imwrite(
        "{}/horizontal_lines.jpg".format(DETECT_LINE_PATH),
        horizontal_lines_img,
    )
    alpha = 0.5
    beta = 1.0 - alpha
    img_final_bin = cv2.addWeighted(
        verticle_lines_img, alpha, horizontal_lines_img, beta, 0.0)
    img_final_bin = cv2.erode(~img_final_bin, kernel, iterations=2)
    (thresh, img_final_bin) = cv2.threshold(
        img_final_bin, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    # For Debugging
    cv2.imwrite(
        "{}/img_final_bin.jpg".format(DETECT_LINE_PATH),
        img_final_bin
    )
    # Find contours for image, which will detect all the boxes
    contours, hierarchy = cv2.findContours(
        img_final_bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # Sort all the contours by top to bottom.
    contours.reverse()
    return contours, img_blur


def predict_value(contours, img_blur, result_dir_path):
    idx = 0
    z = 0
    dx = -1
    # dy = 0
    cXpre = 10000
    Arr = []
    r = 0
    is_scores = 0
    cY_g = 0
    check = True
    check_arr = 0
    y_pre = 0
    # font = cv2.FONT_HERSHEY_SIMPLEX
    real = 0
    for c in contours:
        # Returns the location and width,height for every contour
        x, y, w, h = cv2.boundingRect(c)
        if (w > 30 and h > 15) and w > h:
            idx += 1
            z += 1
            M = cv2.moments(c)
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])

            while(check_arr < 2):
                if(cY > y_pre):
                    check_arr += 1
                    y_pre += 50

            if real < 10:
                real += 1
                cY_g = y
                cXpre = cX
                continue
            if cY_g + 10 < y:
                if cX - cXpre < 0:
                    dx = dx + 1
                    Arr.append([])
                    is_scores = 0
                    r = 0
                cXpre = cX
                new_img = img_blur[y + 3:y + h - 3, x + 3:x + w - 3]

                im, thre = cv2.threshold(
                    new_img, 170, 255, cv2.THRESH_BINARY_INV)
                path = os.path.join(result_dir_path, str(idx) + '.jpg')
                cv2.imwrite(path, thre)
                # cv2.imwrite('./Cropped/'+'a'+str(idx)+'.jpg', thre)
                contours_s, hierachy = cv2.findContours(
                    thre, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
                # rects = [cv2.boundingRect(cnt) for cnt in contours_s]
                s = ''
                contours_s.sort(key=findx)
                a = []
                x_pre = None
                h_pre = None
                check = True
                path = ''
                if contours_s is not None:
                    while (check and (contours_s is not None)):
                        if contours_s:
                            c = contours_s.pop(0)
                        (x, y, w, h) = cv2.boundingRect(c)
                        if x + y != 0:
                            a.append(c)
                            x_pre = x
                            h_pre = h
                            check = False

                    x_pre, h_pre, a = check_diff(
                        contours_s, x_pre, h_pre, a)
                    for con in a:
                        (x, y, w, h) = cv2.boundingRect(con)
                        if (w > 10 and w < 40 and h > 20 and y > 0):
                            idx = idx + 1
                            impb = segment_charactor(new_img, x, y, h, w, idx)
                            try:
                                model_prediction = model.predict_image(impb)
                            except Exception:
                                model_prediction = ""
                            st = model_prediction
                            s = s + str(st)
                            path = os.path.join(
                                "output/Dataset/", str(st), f"{str(idx)}.jpg")

                            cv2.imwrite(path, impb)
                if is_scores > 1:
                    r = len(s)
                    if r > 1:
                        s = s[:1] + "." + s[1:]
                Arr[dx].append(s)
                is_scores += 1
    return Arr


def findx(item):
    item, _, _, _ = cv2.boundingRect(item)
    return item


def segment_charactor(image, x, y, h, w, idx):
    try:
        roi = image[y - 2:y + h + 2, x - 2:x + w + 2]
        thre, im_bin_e = cv2.threshold(
            roi, 180, 255, cv2.THRESH_BINARY)
        h, w = im_bin_e.shape[0], im_bin_e.shape[1]
        wb = int((h - w) / 2)
        impb = np.zeros((h, h)) + 255
        impb = impb.astype('uint8')
        impb[:, wb:wb + w] = im_bin_e

        cv2.imwrite(
            "{0}/{1}.jpg".format(SEGMENTATION_RESULT, str(idx)), impb
        )
    except Exception:
        try:
            im_bin_e = image[y:y + h, x:x + w]
            thre, im_bin_e = cv2.threshold(
                im_bin_e, 180, 255, cv2.THRESH_BINARY)
            h, w = im_bin_e.shape[0], im_bin_e.shape[1]
            wb = int((h - w) / 2)
            impb = np.zeros((h, h)) + 255
            impb = impb.astype('uint8')
            impb[:, wb:wb + w] = im_bin_e
            cv2.imwrite(
                "{0}/{1}.jpg".format(
                    SEGMENTATION_RESULT, str(idx)),
                impb
            )
        except Exception:
            im_bin_e = image[y:y + h, x:x + w]
            thre, impb = cv2.threshold(
                im_bin_e, 180, 255, cv2.THRESH_BINARY)
            cv2.imwrite(
                "{0}/{1}.jpg".format(
                    SEGMENTATION_RESULT, str(idx)),
                impb
            )
    return impb


def check_diff(contours_s, x_pre, h_pre, a):
    for i in contours_s:
        (x, y, w, h) = cv2.boundingRect(i)
        if (x + y != 0):
            if x - x_pre < 13:
                if h > h_pre:
                    a.pop()
                    a.append(i)
                    x_pre = x
                    h_pre = h
            else:
                a.append(i)
                x_pre = x
                h_pre = h
    return x_pre, h_pre, a


def write_to_excel(data, result_excel):
    df_data = []
    length = 0
    for result in data:
        if len(result) >= length:
            length = len(result)
            result = result[:-1]
            total_point = result[len(result) - 1]
            result.append(float_number_to_word(total_point))
        df_data.append(result)

    df = pd.DataFrame(data=df_data)
    df.to_excel(result_excel, header=None, index=False, encoding="utf-8")


def float_number_to_word(number):
    if len(number) == 1:
        return NUMBER_MAPPING[str(number)]
    data = str(number).split(".")
    result = ""
    for number in data:
        result = " ".join([result, NUMBER_MAPPING[number]])
    return result
