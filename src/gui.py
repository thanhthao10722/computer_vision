import sys
import os
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtWidgets import QFileDialog, QWidget
from PyQt5.QtGui import QPixmap
from image_to_excel import submit_result
from cfg import (
    UPLOAD_HEADER,
    UPLOAD_IMG,
    IMG_WARN,
    HEADER_WARN,
    NAME_QLABEL_STYLE,
    SLOGAN_QLABEL_STYLE,
    UPLOAD_ICON_STYLE,
    DESCRIPTION_STYLE,
    EXCEL_FILE_STYLE,
    SUBMIT_BTN_STYLE,
    RESULT_BTN_STYLE,
    FOOTER_STYLE,
    ICON_EXCEL_STYLE,
    DEFAULT_SAVED_FILENAME
)


class Ui_MainWindow(QWidget):
    def open_image_file(self):
        self.submit_btn.show()
        self.result_btn.hide()
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_path, _ = QFileDialog.getOpenFileName(
            self,
            "QFileDialog.getOpenFileName()",
            "",
            " Image files (*.jpg *.gif *.png)",
            options=options
        )
        if file_path:
            file = open(file_path)
            self.description.setText(file_path)
            file.close()

    def open_excel_file(self):
        self.submit_btn.show()
        self.result_btn.hide()
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_path, _ = QFileDialog.getOpenFileName(
            self,
            "QFileDialog.getOpenFileName()",
            "",
            " Excel files (*.xlsx)",
            options=options
        )
        if file_path:
            file = open(file_path)
            self.excel_file.setText(file_path)
            file.close()

    def submit(self):
        header_path = self.excel_file.text()
        image_path = self.description.text()
        if image_path in [UPLOAD_IMG, IMG_WARN]:
            self.description.setText(IMG_WARN)
        if header_path in [UPLOAD_HEADER, HEADER_WARN]:
            self.excel_file.setText(HEADER_WARN)
        if image_path not in [UPLOAD_IMG, IMG_WARN] and \
                header_path not in [UPLOAD_HEADER, HEADER_WARN]:
            name = QFileDialog.getSaveFileName(
                self, "Save File", DEFAULT_SAVED_FILENAME)
            submit_result(header_path, image_path, name[0])
            self.result_btn.show()
            self.submit_btn.hide()
            self.result_btn.setText(name[0])

    def open_saved_file(self):
        os.system(f"libreoffice {self.result_btn.text()}")

    def setup_ui(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowTitle("Welcome to Image to Excel")
        MainWindow.resize(1200, 800)

        self.widget = QtWidgets.QWidget(MainWindow)
        self.widget.setObjectName("widget")
        self.widget.setStyleSheet("background-color:#cdd1ce;")

        # create main layout
        self.layout = QtWidgets.QVBoxLayout()

        # create first layout
        self.first_layout = QtWidgets.QVBoxLayout()
        self.first_layout.setObjectName("first_layout")
        self.first_layout.setAlignment(QtCore.Qt.AlignCenter)

        # create second layout
        self.second_layout = QtWidgets.QHBoxLayout()
        self.second_layout.setObjectName("second_layout")
        self.second_layout.setAlignment(QtCore.Qt.AlignCenter)

        # create third hidden layout for process
        self.third_layout = QtWidgets.QHBoxLayout()
        self.third_layout.setObjectName("third_layout")
        self.third_layout.setAlignment(QtCore.Qt.AlignCenter)

        self.fourth_layout = QtWidgets.QHBoxLayout()
        self.fourth_layout.setObjectName("fourth_layout")
        self.fourth_layout.setAlignment(QtCore.Qt.AlignCenter)

        self.last_layout = QtWidgets.QVBoxLayout()
        self.last_layout.setObjectName("last layout")
        self.last_layout.setAlignment(QtCore.Qt.AlignCenter)

        self.image = QtWidgets.QLabel()
        self.pixmap = QPixmap('image/logo.png')
        self.image.setPixmap(self.pixmap)

        self.name = QtWidgets.QLabel("Image to Excel")
        self.name.setStyleSheet(NAME_QLABEL_STYLE)

        self.slogan = QtWidgets.QLabel("Work Smarter, not Harder")
        self.slogan.setStyleSheet(SLOGAN_QLABEL_STYLE)
        self.slogan.setAlignment(QtCore.Qt.AlignCenter)

        self.first_layout.addWidget(self.image)
        self.first_layout.addWidget(self.name)
        self.first_layout.addWidget(self.slogan)

        self.icon_upload = QtWidgets.QPushButton()
        self.icon_upload.setStyleSheet(UPLOAD_ICON_STYLE)
        self.icon_upload.setIcon(QtGui.QIcon("image/upload_image.png"))
        self.icon_upload.setIconSize(QtCore.QSize(35, 35))
        self.icon_upload.clicked.connect(self.open_image_file)

        self.description = QtWidgets.QLabel(UPLOAD_IMG)
        self.description.setStyleSheet(DESCRIPTION_STYLE)

        self.second_layout.addWidget(self.icon_upload)
        self.second_layout.addWidget(self.description)

        self.icon_excel = QtWidgets.QPushButton()
        self.icon_excel.setStyleSheet(ICON_EXCEL_STYLE)
        self.icon_excel.setIcon(QtGui.QIcon("image/excel.png"))
        self.icon_excel.setIconSize(QtCore.QSize(35, 35))
        self.icon_excel.clicked.connect(self.open_excel_file)

        self.excel_file = QtWidgets.QLabel(UPLOAD_HEADER)
        self.excel_file.setStyleSheet(EXCEL_FILE_STYLE)
        self.third_layout.addWidget(self.icon_excel)
        self.third_layout.addWidget(self.excel_file)

        self.submit_btn = QtWidgets.QPushButton("Submit")
        self.submit_btn.setStyleSheet(SUBMIT_BTN_STYLE)
        self.submit_btn.clicked.connect(self.submit)

        self.result_btn = QtWidgets.QPushButton()
        self.result_btn.setStyleSheet(RESULT_BTN_STYLE)
        self.result_btn.clicked.connect(self.open_saved_file)
        self.result_btn.hide()
        self.fourth_layout.addWidget(self.submit_btn)
        self.last_layout.addWidget(self.result_btn)

        self.lb_footer = QtWidgets.QLabel("Version 2020.01")
        self.lb_footer.setStyleSheet(FOOTER_STYLE)
        self.lb_footer.setAlignment(QtCore.Qt.AlignCenter)

        self.layout.addLayout(self.first_layout)
        self.layout.addLayout(self.second_layout)
        self.layout.addLayout(self.third_layout)
        self.layout.addLayout(self.fourth_layout)
        self.layout.addLayout(self.last_layout)
        self.layout.addWidget(self.lb_footer)
        self.widget.setLayout(self.layout)

        MainWindow.setCentralWidget(self.widget)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setup_ui(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
