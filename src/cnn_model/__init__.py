from .model import CNNModel
from cfg import SAVED_MODEL_PATH


def __sif__():
    model = CNNModel(SAVED_MODEL_PATH)
    return model


model = __sif__()

__all__ = (
    "model"
)
