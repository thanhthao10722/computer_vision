import os
import numpy as np
from PIL import Image
from sklearn.preprocessing import label_binarize
from keras.utils import np_utils
from keras.datasets import mnist
from cfg import TRAINING_DIR, CLASS_NAMES, IMAGE_SIZE


def load_data(path):
    X_data = []
    y_data = []
    filenames = []
    for dirname in os.listdir(path):
        if dirname in CLASS_NAMES:
            classdir = path + '/' + dirname
            for filename in os.listdir(classdir):
                if filename.endswith('.jpg') or filename.endswith('.png'):
                    img = Image.open(f"{classdir}/{filename}").convert("L")
                    img = img.resize(IMAGE_SIZE, Image.ANTIALIAS)
                    if (img.size != IMAGE_SIZE):
                        continue
                    im2arr = np.array(img)
                    array = np.full(IMAGE_SIZE, 255) - im2arr
                    X_data.append(array)
                    y_data.append(dirname)
                    filenames.append(f"{classdir}/{filename}")
    file_info = {
        "input_label": y_data,
        "file_path": filenames
    }
    X_data = np.reshape(X_data, (np.size(y_data), 28, 28, 1))
    y_data = np.reshape(y_data, (np.size(y_data), 1))
    y_data = label_binarize(y_data, classes=CLASS_NAMES)
    return X_data, y_data, file_info


def prepare_data():
    (X_mnist, y_mnist), (X_test, y_test) = mnist.load_data()
    X_val_mnist, y_val_mnist = X_mnist[50000:60000, :], y_mnist[50000:60000]
    X_mnist, y_mnist = X_mnist[:10000, :], y_mnist[:10000]

    X_mnist = X_mnist.reshape(X_mnist.shape[0], 28, 28, 1)
    y_mnist = np_utils.to_categorical(y_mnist, 10)

    X_data, y_data, _ = load_data(TRAINING_DIR)
    X_train = np.vstack([X_mnist, X_data])
    y_train = np.vstack([y_mnist, y_data])

    X_val = X_val_mnist.reshape(X_val_mnist.shape[0], 28, 28, 1)
    y_val = np_utils.to_categorical(y_val_mnist, 10)

    return X_train, y_train, X_val, y_val


def load_image_for_predict(path):
    X_data = []
    filenames = []
    for filename in os.listdir(path):
        if filename.endswith('.jpg') or filename.endswith('.png'):
            img = Image.open(f"{path}/{filename}").convert("L")
            img = img.resize(IMAGE_SIZE, Image.ANTIALIAS)
            if (img.size != IMAGE_SIZE):
                continue
            im2arr = np.array(img)
            array = np.full(IMAGE_SIZE, 255) - im2arr
            X_data.append(array)
            filenames.append(f"{path}/{filename}")
    X_data = np.reshape(X_data, (len(X_data), 28, 28, 1))
    return X_data, filenames


def load_image(path):
    img = Image.open(f"{path}").convert("L")
    img = img.resize(IMAGE_SIZE, Image.ANTIALIAS)
    im2arr = np.array(img)
    array = np.full(IMAGE_SIZE, 255) - im2arr
    return array


def preprocess_image(image):
    image = image.resize(IMAGE_SIZE, Image.ANTIALIAS)
    image.convert('L')
    im2arr = np.array(image)
    array = np.full(IMAGE_SIZE, 255) - im2arr
    return array
