import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.layers import Conv2D, MaxPooling2D
from helper import prepare_data
from keras.callbacks import ModelCheckpoint
from cfg import SAVED_MODEL_PATH, OUTPUT_PATH, SAVED_PREDICT_RESULT
import numpy as np
import os
from keras.optimizers import Adam
import logging

# 1. Load dữ liệu
X_train, y_train, X_val, y_val = prepare_data()

# 2. Create model
model = Sequential()
# Thêm Convolutional layer với 32 kernel, kích thước kernel 3*3
# dùng hàm relu làm activation và chỉ rõ input_shape cho layer đầu tiên
model.add(Conv2D(32, (3, 3), activation='relu', input_shape=(28, 28, 1)))

# Thêm Convolutional layer
model.add(Conv2D(32, (3, 3), activation='relu'))

# Thêm Max pooling layer
model.add(MaxPooling2D(pool_size=(2, 2)))

# Flatten layer chuyển từ tensor sang vector
model.add(Flatten())

# Thêm Fully Connected layer với 128 nodes và dùng hàm relu
model.add(Dense(128, activation='relu'))

# Output layer với 10 node và dùng softmax function để chuyển sang xác suất.
model.add(Dense(10, activation='softmax'))

# 3. Compile model, chỉ rõ hàm loss_function nào được sử dụng, phương thức
# đùng để tối ưu hàm loss function.

checkpoint = ModelCheckpoint(SAVED_MODEL_PATH,
                             monitor='val_loss',
                             verbose=0,
                             save_best_only=True,
                             mode='auto')
epochs = 10
opt = Adam(lr=0.00001, decay=0.00001 / epochs)
model.compile(loss='categorical_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])

# 4. Thực hiện train model với data
H = model.fit(
    X_train,
    y_train,
    validation_data=(X_val, y_val),
    batch_size=32,
    epochs=epochs,
    verbose=1,
    callbacks=[checkpoint])

logging.info(model.summary())
# 5. Vẽ đồ thị
if not os.path.isdir(OUTPUT_PATH):
    os.mkdir(OUTPUT_PATH)
if not os.path.isdir(SAVED_PREDICT_RESULT):
    os.mkdir(SAVED_PREDICT_RESULT)
fig = plt.figure()
plt.plot(np.arange(0, epochs), H.history["loss"], label="loss")
plt.plot(np.arange(0, epochs),
         H.history["val_loss"], label="validation loss")
plt.plot(np.arange(0, epochs), H.history["accuracy"], label="accuracy")
plt.plot(np.arange(0, epochs),
         H.history["val_accuracy"], label="validation accuracy")
plt.title("Accuracy and Loss")
plt.xlabel("Epoch")
plt.ylabel("Loss|Accuracy")
plt.legend()
plt.savefig(f"{SAVED_PREDICT_RESULT}/cnn_model_history.png")
plt.show()
