import keras
from PIL import Image
from .helper import load_data, preprocess_image
from cfg import SAVED_MODEL_PATH, TEST_DIR, IMG_SHAPE, IMG_CHANNEL
import numpy as np
import pandas as pd


class CNNModel():
    def __init__(self, saved_model_path=SAVED_MODEL_PATH):
        self.model = keras.models.load_model(saved_model_path)

    def predict(self, saved_prediction_path, test_path=TEST_DIR):
        prediction_labels = []
        indexs = []
        predictions = []
        X_test, _, file_info = load_data(test_path)
        for index, x_test in enumerate(X_test):
            y_predict = self.model.predict(x_test.reshape(
                IMG_CHANNEL, IMG_SHAPE, IMG_SHAPE, IMG_CHANNEL))
            prediction_labels.append(np.argmax(y_predict))
            predictions.append(y_predict)
            indexs.append(index)
        data = {
            "file_path": file_info["file_path"],
            "input_label": file_info["input_label"],
            "prediction_label": prediction_labels,
            "prediction": predictions
        }
        data_frame = pd.DataFrame(data, index=indexs)
        data_frame.to_csv(saved_prediction_path, index=False, header=True)

    def evaluate(self, test_path=TEST_DIR, verbose=0):
        X_test, y_test, _ = load_data(test_path)
        score = self.model.evaluate(X_test, y_test, verbose=verbose)
        print("Evaluate result:", score)

    def predict_image(self, img):
        loaded_image = Image.fromarray(img)
        image = preprocess_image(loaded_image)
        y_predict = self.model.predict(image.reshape(
            IMG_CHANNEL, IMG_SHAPE, IMG_SHAPE, IMG_CHANNEL))
        result = np.argmax(y_predict)
        return result
