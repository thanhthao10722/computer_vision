clean-py-binary:
	find ./src ./cfg -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

install:
	pip install -r requirements.txt

run:
	PYTHONPATH=./src python -m gui

train:
	PYTHONPATH=./src/cnn_model python -m train_model

image_to_excel:
	PYTHONPATH=./src python -m image_to_excel