DETECT_LINE_PATH = "output/detect_line"
DETECT_LETTER_PATH = "output/detect_letter"
INPUT_PATH = "input"
OUTPUT_PATH = "output"
TRAINING_DIR = "dataset/training"
TEST_DIR = "dataset/test"
CLASS_NAMES = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
SAVED_MODEL_PATH = "src/cnn_model/cnn_model.h5"
SAVED_PREDICT_RESULT = "output/predict_cnn_model"
SEGMENTATION_RESULT = "output/segmentation_result"
EXCEL_RESULT = "output/excel_result"
FILE_EXCEL_RESULT = "output/excel_result/result.xlsx"
UPLOAD_IMG = "Upload your image here"
UPLOAD_HEADER = "Upload your header here"
IMG_WARN = "Please upload your image!"
HEADER_WARN = "Please upload your header!"
NUMBER_MAPPING = {
    "0": "Không", "1": "Một", "2": "Hai", "3": "Ba",
    "4": "Bốn", "5": "Năm", "6": "Sáu", "7": "Bảy",
    "8": "Tám", "9": "Chín"
}
IMAGE_SIZE = (28, 28)
IMG_CHANNEL = 1
IMG_SHAPE = 28

DEFAULT_SAVED_FILENAME = "result.xlsx"

NAME_QLABEL_STYLE = "QLabel { \
                border-color: beige; font-size: 45px;\
                color: black;\
            }"

SLOGAN_QLABEL_STYLE = "QLabel { \
                border-color: beige; font-size: 17px;\
                color: black;\
            }"

UPLOAD_ICON_STYLE = "QPushButton {\
                margin-top: 60px;\
                border: none;\
            }"
DESCRIPTION_STYLE = "QLabel {\
                font-size: 16px;\
                margin-top: 60px;\
                color: black;\
            }"

EXCEL_FILE_STYLE = "QLabel {\
                font-size: 16px;\
                color: black;\
            }"
SUBMIT_BTN_STYLE = "QLabel {\
                background-color: white; \
                border-style:outset;\
                border-color: beige; font-size: 16px;\
                padding: 6px;\
                color: black;\
                height: 10px;\
                width: 20px;\
            }"
RESULT_BTN_STYLE = "QPushButton {\
                background-color: #cdd1ce; \
                font-size: 16px;\
                border-color: beige;\
                color: #fc0356;\
                border: None;\
            }"
FOOTER_STYLE = "QLabel {\
                background-color: white; \
                border-style:outset;\
                border-color: beige; font-size: 17px;\
                min-width: 10em; padding: 6px;\
                color: black;width: 13px;\
                height: 15px;\
            }"

ICON_EXCEL_STYLE = "QPushButton {\
                border: none;\
            }"
